<div id="welcome">
	<div class="row">
		<div class="container">
			<div class="wlcLeft col-5 fl">
				<img src="public/images/content/wlcIMG1.png" alt="cabinet">
			</div>
			<div class="wlcRight col-7 fr">
				<h3>WELCOME TO</h3>
				<h1>IVY DESIGN INC.</h1>
				<p>Since 2003, IVY Design, Inc has grown into a diverse team with professionals and experts, all with different expertise and stories to tell. When our product arrives on your doorstep, you’re witness to the tender love and care every one of our team has poured into delivering the product.</p>
				<a href="services#content" class="btn">LEARN MORE</a>
				<a href="contact#content" class="btn">CONTACT US</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="section2">
	<div class="row">
		<div class="sec2Left col-5 fl">
			<h2>WORLD CLASS FURNITURE</h2>
			<h3>FOR EVERY HOME</h3>
			<p class="desc">In 2003, we startedIVY Design with a vision — <span>to make a million homes beautiful.</span></p>
			<p class="sm">
				<a href="<?php $this->info("fb_link"); ?>" class="social" target="_blank">f</a>
				<a href="<?php $this->info("tt_link"); ?>" class="social" target="_blank">l</a>
				<a href="<?php $this->info("yt_link"); ?>" class="social" target="_blank">x</a>
				<a href="<?php $this->info("rss_link"); ?>" class="social" target="_blank">r</a>
			</p>
		</div>
		<img src="public/images/content/section2IMG.jpg" alt="counter" class="counter">
		<div class="clearfix"></div>
	</div>
</div>
<div id="services">
	<div class="row">
		<h2>OUR SERVICES</h2>
		<p>Exclusive range of living room furniture and home furniture online at best prices!</p>
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/service1.jpg" alt="Service Image 1"> </dt>
				<dd>ANY KIND OF WOOD <span>WORKING</span></dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service2.jpg" alt="Service Image 2"> </dt>
				<dd>SHAGREEN PARCHMENT</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service3.jpg" alt="Service Image 3"> </dt>
				<dd>MIKA PARCHMENT</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service4.jpg" alt="Service Image 4"> </dt>
				<dd>FURNITURE FINISHING</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/service5.jpg" alt="Service Image 5"> </dt>
				<dd>FURNITURE DESIGN</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section4">
	<div class="row">
		<div class="why">
			<h3>OUR VALUES</h3>
			<h2>WHY CHOOSE US</h2>
			<p>
				QUALITY
				<span>We offer quality furniture, because we believe that if something is worth doing, it’s worth doing well.</span>
			</p>
			<img src="public/images/content/we-sprites.png" alt="we" class="we">
				<img src="public/images/content/weIMG1.png" alt="counter" class="weImg">
		</div>
		<div class="gallery">
			<h2>OUR GALLERY</h2>
			<div class="container">
				<img src="public/images/content/gallery1.jpg" alt="gallery 1">
				<img src="public/images/content/gallery2.jpg" alt="gallery 2">
				<img src="public/images/content/gallery3.jpg" alt="gallery 3">
			</div>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>CONTACT US</h2>
		<p>We will be glad to answer your questions, feel free to ask a piece of information or a quotation.<span>We are looking forward to work with you.</span></p>
		<div class="container">
			<dl>
				<dt> <img src="public/images/common/sprite.png" alt="email" class="email"> </dt>
				<dd><?php $this->info(["email","mailto"]); ?></dd>
			</dl>
			<dl>
				<dt> <img src="public/images/common/sprite.png" alt="phone" class="phone"> </dt>
				<dd><?php $this->info(["phone","tel","txtPhone"]); ?></dd>
			</dl>
			<dl>
				<dt> <img src="public/images/common/sprite.png" alt="location" class="location"> </dt>
				<dd><?php $this->info("address"); ?></dd>
			</dl>
		</div>

	</div>
</div>
<div id="section6">
	<div class="row">
		<div class="s6Left col-6 fl">
			<img src="public/images/common/cabinet.png" alt="cabinet">
		</div>
		<div class="s6Right col-6 fr">
			<a href="<?php echo URL; ?>">
				<img src="public/images/common/footLogo.png" alt="footLogo">
			</a>
			<img src="public/images/content/cabinet2.jpg" alt="cabinet" class="cabinet2">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
